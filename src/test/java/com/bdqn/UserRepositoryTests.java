package com.bdqn;

import com.bdqn.entity.User;
import com.bdqn.pojo.UserInfo;
import com.bdqn.repository.UserRepository;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

@SpringBootTest
class UserRepositoryTests {

    /**
     * 引入JPA数据访问层
     */
    @Resource
    private UserRepository userRepository;

    /**
     * 测试查询所有数据
     */
    @Test
    public void testFindAll() {
        List<User> list = userRepository.findAll();
        for (User user : list) {
            System.out.println(user);
        }
    }

    /**
     * 测试根据ID查询
     */
    @Test
    public void testFindById() {
        User user = userRepository.findById(4L).get();
        System.out.println(user);
    }

    /**
     * 测试新增数据
     */
    @Test
    public void testInsert() {
        User user = new User("小红老师", "123456", 1L, 1);
        User save = userRepository.save(user);
        System.out.println(save);
    }

    /**
     * 测试修改数据
     */
    @Test
    public void testUpdate() {
        User user = userRepository.findById(84L).get();
        user.setUsrRoleId(3L);
        User save = userRepository.save(user);
        System.out.println(save);
    }

    /**
     * 测试删除数据
     */
    @Test
    public void testDelete() {
        userRepository.deleteById(83L);
        System.out.println("删除成功");
    }

    /**
     * 测试根据用户名查找用户列表
     */
    @Test
    public void testFindByUsrName() {
        // 自定义方法查询
        List<User> list = userRepository.findByUsrName("小鸡");
        for (User user : list) {
            System.out.println(user);
        }
    }

    /**
     * 测试根据用户名模糊查找用户列表
     */
    @Test
    public void testFindByUsrNameLike() {
        List<User> list = userRepository.findByUsrNameLike("%小帅%");
        for (User user : list) {
            System.out.println(user);
        }
    }

    /**
     * 测试通过JPA查询数据
     */
    @Test
    public void testFindByUsrNameLikeAndUsrRoleIdAndUsrFlag() {
        // 调用JPA的自定义方法查询对象
        List<User> list = userRepository.findByUsrNameLikeAndUsrRoleIdAndUsrFlag("%老师%", 2L, 1);
        for (User user : list) {
            System.out.println(user);
        }
    }

    /**
     * 测试使用自定义HQL语句查询
     */
    @Test
    public void testFindByRoleIdHQL() {
        List<User> list = userRepository.findByRoleIdHQL(2L);
        for (User user : list) {
            System.out.println(user);
        }
    }

    /**
     * 测试使用自定义SQL语句查询
     */
    @Test
    public void testFindByRoleIdSQL() {
        List<User> list = userRepository.findByRoleIdSQL(2L);
        for (User user : list) {
            System.out.println(user);
        }
    }

    /**
     * 测试自定义HQL修改用户
     */
    @Test
    public void testModifyNameById() {
        int row = userRepository.modifyNameById("小帅老师666", 84L);
        if (row >= 1) {
            System.out.println("修改成功");
        } else {
            System.out.println("修改失败");
        }
    }

    /**
     * 测试命名查询
     */
    @Test
    public void testFindUsersByName() {
        List<User> list = userRepository.findUsersByName("小帅老师666");
        for (User user : list) {
            System.out.println(user);
        }
    }

    /**
     * 测试分页查询
     */
    @Test
    public void testFindPageByUsrRoleId() {
        Pageable pageable = PageRequest.of(0, 3, Sort.by(Sort.Direction.DESC, "usrId"));
        Page<User> userPage = userRepository.findPageByUsrRoleId(2L, pageable);
        System.out.println("总记录数：" + userPage.getTotalElements());
        System.out.println("总页数：" + userPage.getTotalPages());
        System.out.println("当前页数：" + (userPage.getNumber() + 1));
        System.out.println("每页记录数：" + userPage.getSize()); // 3
        System.out.println("当前页记录数：" + userPage.getNumberOfElements()); // 1
        for (User user : userPage.getContent()) {
            System.out.println(user);
        }
    }

    /**
     * 测试使用接口作为返回数据
     */
    @Test
    public void testUserInfo() {
        UserInfo userInfo = userRepository.getUserInfo(84L);
        System.out.println("getUsrId: " + userInfo.getUsrId());
        System.out.println("getUsrName: " + userInfo.getUsrName());
        System.out.println("getUsrRoleId: " + userInfo.getUsrRoleId());
        System.out.println("getRoleName: " + userInfo.getRoleName());
    }

}
