package com.bdqn;

import com.bdqn.pojo.SysRole;
import com.bdqn.pojo.SysUser;
import com.bdqn.repository.SysRoleRepository;
import com.bdqn.repository.SysUserRepository;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SysRoleRepositoryTests {

    /**
     * 引入用户数据访问层
     */
    @Resource
    private SysUserRepository sysUserRepository;

    /**
     * 引入角色数据访问层
     */
    @Resource
    private SysRoleRepository sysRoleRepository;

    /**
     * 测试多对一查询（通过@ManyToOne注解绑定多对一关系）
     */
    @Test
    public void testManyToOne() {
        SysUser sysUser = sysUserRepository.findById(84L).get();
        System.out.println(sysUser);
    }

    /**
     * 测试一对多查询（通过@OneToMany注解绑定一对多关系）
     */
    @Test
    public void testOneToMany() {
        SysRole sysRole = sysRoleRepository.findById(2L).get();
        System.out.println(sysRole);
    }

    /**
     * 测试通过JPA级联新增
     */
    @Test
    public void testOneToManyAdd() {
        SysRole sysRole = new SysRole("T369测试角色", "演示级联新增角色和用户", 1);
        SysUser sysUser1 = new SysUser("小曹", "123456", 1, sysRole);
        SysUser sysUser2 = new SysUser("小月", "123456", 1, sysRole);
        sysRole.getUsers().add(sysUser1);
        sysRole.getUsers().add(sysUser2);
        SysRole save = sysRoleRepository.save(sysRole);
        System.out.println(save.getRoleId());
    }

    /**
     * 测试通过JPA级联删除
     */
    @Test
    public void testOneToManyDelete() {
        // 先使用 getOne 方法获取到 SysRole 的引用，然后调用 delete 方法删除
        SysRole sysRole = sysRoleRepository.getOne(34L);
        sysRoleRepository.delete(sysRole);
        System.out.println("删除成功");
    }

}
