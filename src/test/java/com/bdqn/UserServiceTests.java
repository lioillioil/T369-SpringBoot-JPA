package com.bdqn;

import com.bdqn.entity.User;
import com.bdqn.service.UserService;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.HashMap;
import java.util.Map;

@SpringBootTest
class UserServiceTests {

    /**
     * 引入JPA数据访问层
     */
    @Resource
    private UserService userService;

    /**
     * 测试多条件分页查询
     */
    @Test
    public void testFindPageByMap() {
        Map<String, Object> param = new HashMap<>();
        param.put("usrName", "小");
        param.put("roleId", 2L);
        Pageable pageable = PageRequest.of(0, 3, Sort.by(Sort.Direction.DESC, "usrId"));
        Page<User> userPage = userService.findPageByMap(param, pageable);
        System.out.println("总记录数：" + userPage.getTotalElements());
        System.out.println("总页数：" + userPage.getTotalPages());
        System.out.println("当前页数：" + (userPage.getNumber() + 1));
        System.out.println("每页记录数：" + userPage.getSize()); // 3
        System.out.println("当前页记录数：" + userPage.getNumberOfElements()); // 1
        for (User user : userPage.getContent()) {
            System.out.println(user);
        }
    }

}
