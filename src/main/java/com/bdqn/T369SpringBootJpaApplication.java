package com.bdqn;

import com.bdqn.pojo.SysUser;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class T369SpringBootJpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(T369SpringBootJpaApplication.class, args);
        SysUser sysUser = new SysUser();
        sysUser.setUsrName("张三");
        System.out.println(sysUser.getUsrName());
    }

}
