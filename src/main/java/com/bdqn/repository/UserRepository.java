package com.bdqn.repository;

import com.bdqn.entity.User;
import com.bdqn.pojo.UserInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 用户数据访问层JPA接口
 *
 * @author LILIBO
 * @since 2024/8/31
 */
public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

    // 什么都不用写就拥有增删改查等通用功能

    // 自定义个性化接口

    /**
     * 根据用户名查找用户列表
     *
     * @param usrName
     * @return
     */
    List<User> findByUsrName(String usrName);

    /**
     * 根据用户名模糊查找用户列表
     *
     * @param usrName
     * @return
     */
    List<User> findByUsrNameLike(String usrName);

    /**
     * 根据用户名和密码查询用户列表
     *
     * @param usrName
     * @param usrRoleId
     * @param usrFlag
     * @return
     */
    List<User> findByUsrNameLikeAndUsrRoleIdAndUsrFlag(String usrName, Long usrRoleId, Integer usrFlag);

    /**
     * 使用自定义HQL语句查询
     *
     * @param roleId
     * @return
     */
    @Query("select u from User u where u.usrRoleId=?1")
    List<User> findByRoleIdHQL(Long roleId);

    /**
     * 使用自定义SQL语句查询（nativeQuery = true 表示使用原生SQL）
     *
     * @param roleId
     * @return
     */
    @Query(value = "select u.* from sys_user u where u.usr_role_id=:roleId", nativeQuery = true)
    List<User> findByRoleIdSQL(@Param("roleId") Long roleId);

    /**
     * 自定义HQL修改用户
     *
     * @param usrName
     * @param usrId
     * @return
     */
    @Transactional(timeout = 10)
    @Modifying
    @Query("update User u set u.usrName=?1 where u.usrId=?2")
    int modifyNameById(String usrName, Long usrId);

    /**
     * 命名查询（在实体类上通过@NamedQueries注解定义命名查询）
     *
     * @param usrName
     * @return
     */
    List<User> findUsersByName(String usrName);

    /**
     * 分页查询（Pageable作为形参，返回类型为Page）
     *
     * @param roleId
     * @param pageable
     * @return
     */
    @Query("select u from User u where u.usrRoleId=?1")
    Page<User> findPageByUsrRoleId(Long roleId, Pageable pageable);

    /**
     * 返回封装接口（注意：此处为 HQL，必须面向实体和属性编写；获取到的属性必须指定别名）
     *
     * @param usrId
     * @return
     */
    @Query("select u.usrId as usrId, u.usrName as usrName, u.usrPassword as usrPassword, u.usrRoleId as usrRoleId, u.usrFlag as usrFlag, r.roleName as roleName from User u, Role r where u.usrRoleId=r.roleId and u.usrId=?1")
    UserInfo getUserInfo(Long usrId);

}
