package com.bdqn.repository;

import com.bdqn.pojo.SysRole;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 角色数据访问层JPA接口
 *
 * @author LILIBO
 * @since 2024/9/3
 */
public interface SysRoleRepository extends JpaRepository<SysRole, Long> {
}
