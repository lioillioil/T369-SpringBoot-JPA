package com.bdqn.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serial;
import java.io.Serializable;

/**
 * 系统用户实体类
 */
@Data // Lombok注解，自动生成Getter/Setter
@NoArgsConstructor // 生成无参构造方法
@AllArgsConstructor // 生成全参构造方法
@ToString // 生成toString方法
@Entity // JPA实体类
@Table(name = "`sys_user`") // 对应表
@NamedQueries(@NamedQuery(name = "User.findUsersByName", query = "select u from User u where u.usrName = ?1")) // 命名查询
public class User implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usr_id")
    private Long usrId;
    @Column(name = "usr_name")
    private String usrName;
    @Column(name = "usr_password")
    private String usrPassword;
    @Column(name = "usr_role_id")
    private Long usrRoleId;
    @Column(name = "usr_flag")
    private Integer usrFlag;
    @Transient // 忽略该属性与数据库表的对应关系
    private String queryStartTime;
    @Transient // 忽略该属性与数据库表的对应关系
    private String queryEndTime;

    public User(String usrName, String usrPassword, Long usrRoleId, Integer usrFlag) {
        this.usrName = usrName;
        this.usrPassword = usrPassword;
        this.usrRoleId = usrRoleId;
        this.usrFlag = usrFlag;
    }

}
