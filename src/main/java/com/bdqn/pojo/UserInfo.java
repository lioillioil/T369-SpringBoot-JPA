package com.bdqn.pojo;

/**
 * UserInfo 用户信息接口
 *
 * @author LILIBO
 * @since 2024/9/3
 */
public interface UserInfo {

    // -- 获取用户信息 --
    Long getUsrId();

    String getUsrName();

    String getUsrPassword();

    Long getUsrRoleId();

    Integer getUsrFlag();

    // -- 获取角色名称 --
    String getRoleName();

}
