package com.bdqn.pojo;

import jakarta.persistence.*;

import java.io.Serial;
import java.io.Serializable;

/**
 * 系统用户实体类
 */
// @Data // Lombok注解，自动生成Getter/Setter
// @NoArgsConstructor // 生成无参构造方法
// @AllArgsConstructor // 生成全参构造方法
// @ToString // 生成toString方法
@Entity // JPA实体类
@Table(name = "`sys_user`") // 对应表
// 命名查询
@NamedQueries(@NamedQuery(name = "SysUser.findUsersByName", query = "select u from SysUser u where u.usrName = ?1"))
public class SysUser implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usr_id")
    private Long usrId;
    @Column(name = "usr_name")
    private String usrName;
    @Column(name = "usr_password")
    private String usrPassword;
    /* @Column(name = "usr_role_id")
    private Long usrRoleId; */
    @Column(name = "usr_flag")
    private Integer usrFlag;
    @Transient // 忽略该属性与数据库表的对应关系
    private String queryStartTime;
    @Transient // 忽略该属性与数据库表的对应关系
    private String queryEndTime;

    /**
     * 多对一关联
     */
    @ManyToOne(targetEntity = SysRole.class) // 关联的目标类型
    @JoinColumn(name = "usr_role_id") // 关联的外键字段
    private SysRole sysRole;

    public SysUser() {
    }

    public SysUser(String usrName, String usrPassword, Integer usrFlag, SysRole sysRole) {
        this.usrName = usrName;
        this.usrPassword = usrPassword;
        this.usrFlag = usrFlag;
        this.sysRole = sysRole;
    }

    @Override
    public String toString() {
        return "SysUser{" +
                "usrId=" + usrId +
                ", usrName='" + usrName + '\'' +
                ", usrPassword='" + usrPassword + '\'' +
                ", usrFlag=" + usrFlag +
                ", sysRole.roleName=" + sysRole.getRoleName() +
                '}'; // 双向关联时要打断循环嵌套打印（在角色方只打印关联的用户数量或者在用户方只打印角色名称）
    }

    public Long getUsrId() {
        return usrId;
    }

    public void setUsrId(Long usrId) {
        this.usrId = usrId;
    }

    public String getUsrName() {
        return usrName;
    }

    public void setUsrName(String usrName) {
        this.usrName = usrName;
    }

    public String getUsrPassword() {
        return usrPassword;
    }

    public void setUsrPassword(String usrPassword) {
        this.usrPassword = usrPassword;
    }

    public Integer getUsrFlag() {
        return usrFlag;
    }

    public void setUsrFlag(Integer usrFlag) {
        this.usrFlag = usrFlag;
    }

    public String getQueryStartTime() {
        return queryStartTime;
    }

    public void setQueryStartTime(String queryStartTime) {
        this.queryStartTime = queryStartTime;
    }

    public String getQueryEndTime() {
        return queryEndTime;
    }

    public void setQueryEndTime(String queryEndTime) {
        this.queryEndTime = queryEndTime;
    }

    public SysRole getSysRole() {
        return sysRole;
    }

    public void setSysRole(SysRole sysRole) {
        this.sysRole = sysRole;
    }

}
