package com.bdqn.service;

import com.bdqn.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;

/**
 * SysUserService
 *
 * @author LILIBO
 * @since 2024/9/3
 */
public interface UserService {

    /**
     * 多条件分页查询
     *
     * @param param
     * @param pageable
     * @return
     */
    Page<User> findPageByMap(Map param, Pageable pageable);

}
